# NestJS - Surya Digital Technical Test Create Users Birthday Reminder

## PRE-REQUIREMENTS (INSTALL THIS ON YOUR LOCALHOST BEFORE RUN THE API)

- Typescript
- PostgreSQL
- Redis

## HOW TO RUN THE API IN LOCALHOST PORT 3000

```bash
npm install

cp .env.example .env

npx prisma generate

npm run migrate:dev:create

npm run migrate:dev

npm run start:dev
```

## Docker File

YOU ALSO ABLE RUN VIA DOCKER IF YOU PREFFER THAT WAY

```bash
docker build -t nest-api .

docker run -p 3000:3000 --env-file .env -d nest-api
```

## Docker Compose

OR DOCKER COMPOSE

```bash
docker-compose up
# or detached
docker-compose up -d
```

## POSTMAN COLLECTION
[Download File](https://drive.google.com/file/d/1vfC0hSazP6s6IrtjhsfdH7cz9CxBnszl/view?usp=share_link)

It's countain 4 main API:

- Create User
- Update User
- Delete User
- Get All User

## CRON & JOB

I use NestJs queue library to handdle task scheduler and NestJs Bull to handle send email job
