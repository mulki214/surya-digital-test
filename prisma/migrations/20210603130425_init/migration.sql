-- CreateTable
CREATE TABLE "Users" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "birthdate" DATE NOT NULL,
    PRIMARY KEY ("id")
);
