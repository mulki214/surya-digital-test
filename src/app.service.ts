import { Injectable, Logger } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { PrismaService } from './prisma/prisma.service';
import { Users } from '@prisma/client';
import { Queue } from 'bull';
import { InjectQueue } from '@nestjs/bull';
import * as moment from 'moment';


@Injectable()
export class AppService {

  private readonly logger = new Logger(AppService.name);
  constructor(
    private readonly prismaService: PrismaService,
    @InjectQueue('mail-queue') private queue: Queue
  ) {}

  // send email job
  async sendMail(data: Users[]){
    await data.map(async (e) => {

    
      const birthdayMoment = moment(e.birthdate)
      const today = moment().startOf('day');


      if ( birthdayMoment.month() === today.month() && birthdayMoment.date() === today.date()) {
        await this.queue.add(
          e,
          {
            delay: 5000
          }
        );
        console.log('done');
      }
    });
  }

  // run task to check birthdate every 9 AM
  @Cron('0 9 * * * *')
  async handleCron() {

    const users:Users[]  = await this.prismaService
      .$queryRaw`SELECT * FROM public."Users"
      WHERE
          DATE_PART('day', birthdate) = date_part('day', CURRENT_DATE)
      AND
          DATE_PART('month', birthdate) = date_part('month', CURRENT_DATE)`;

    await this.sendMail(users);

  }
}
