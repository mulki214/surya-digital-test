import { PrismaService } from './prisma/prisma.service';
import { Controller, Delete, Get, Param, Post, Put, Req } from '@nestjs/common';
import { Users } from '@prisma/client';
import * as momenttz from 'moment-timezone';

@Controller()
export class AppController {
  constructor(
    private readonly prismaService: PrismaService,
  ) {}

  // GET ALL USER
  @Get('user')
  async getUsers(): Promise<Users[]> {
    return this.prismaService.users.findMany();
  }

  // CREATE USER
  @Post('user')
  async postUsers(@Req() req: any) {
    let { firstname, lastname, birthdate, email } = req.body;
    const currentTimezone = momenttz.tz.guess();
    const location = momenttz.tz.zone(currentTimezone).name;
    
    birthdate = new Date(birthdate);

    await this.prismaService.users.create({
      data: {
        firstname,
        lastname,
        email,
        birthdate,
        location
      },
    });
    return this.prismaService.users.findMany();
  }

  // DELETE USER
  @Delete('user/:id')
  async remove(@Param('id') id: number) {
    await this.prismaService.users.delete({
      where: {
        id: Number(id),
      },
    })
    return this.prismaService.users.findMany();
  }

  // EDIT USER
  @Put('user')
  async putUsers(@Req() req: any) {
    let { id, firstname, lastname, birthdate, email } = req.body;
    const currentTimezone = momenttz.tz.guess();
    const location = momenttz.tz.zone(currentTimezone).name;
    
    birthdate = new Date(birthdate);

    await this.prismaService.users.update({
      where: {
        id: id,
      },
      data:{
        firstname,
        lastname,
        email,
        birthdate,
        location
      },
    })
    return this.prismaService.users.findMany();
  }  
}
