import { HttpService } from "@nestjs/axios";
import { Process, Processor } from "@nestjs/bull";
import { Job } from "bull";

@Processor('mail-queue')
export class MailConsumer {

  constructor(
    private readonly httpService: HttpService
  ){}

  // procecess mail job
  @Process()
  async mailJob(job: Job<any>){

      const fullname = `${job.data.firstname} ${job.data.lastname}`;

      const sent = await this.httpService.axiosRef.post('https://email-service.digitalenvision.com.au/send-email',{

        "email": job.data.email,
        "message": `Hey, ${fullname} it's your birthday`

      });
      
      console.log(sent);
  
  }

}