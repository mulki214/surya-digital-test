import { PrismaModule } from './prisma/prisma.module';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ScheduleModule } from '@nestjs/schedule';
import { HttpModule } from '@nestjs/axios';
import { BullModule } from '@nestjs/bull';
import { MailConsumer } from './mail.consumer';

@Module({
  imports: [
    PrismaModule, 
    ScheduleModule.forRoot(), 
    BullModule.registerQueue({
      name: 'mail-queue',
      redis: {
        host: 'localhost',
        port: 6379,
      },
    }),
    HttpModule
  ],
  controllers: [AppController],
  providers: [
    AppService,
    MailConsumer
  ],
})
export class AppModule {}
